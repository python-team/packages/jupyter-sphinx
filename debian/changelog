jupyter-sphinx (0.5.3-1) UNRELEASED; urgency=medium

  [ Picca Frédéric-Emmanuel ]
  * New upstream release.
  * d/control: switch to hatchling for build tool.
  * d/control: Added RRR=no
  * Bug fix: "package relies on unavailable `ipywidgets.embed` module",
    thanks to Sebastian Luque (Closes: #950598).

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Wed, 20 Mar 2024 06:25:07 +0100

jupyter-sphinx (0.3.2-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Update standards version to 4.6.0, no changes needed.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 29 Nov 2022 19:40:56 +0000

jupyter-sphinx (0.3.2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

  [ Picca Frédéric-Emmanuel ]
  * New upstream version 0.3.2

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Thu, 02 Sep 2021 10:21:09 +0200

jupyter-sphinx (0.2.3-1) unstable; urgency=medium

  [ Alexandre Marie ]
  * Updated vcs-git url.
  * Added the d/salsa-ci.yml file for Salsa-CI.

  [ Picca Frédéric-Emmanuel ]
  * New upstream version 0.2.3
  * d/control: Updated Vcs-X
  * Trim trailing whitespace.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Tue, 24 Dec 2019 09:48:13 +0100

jupyter-sphinx (0.1.4-1) unstable; urgency=low

  * Initial release (Closes: #942846)

 -- Alexandre Marie <alexandre.marie@synchrotron-soleil.fr>  Tue, 22 Oct 2019 15:17:09 +0200
